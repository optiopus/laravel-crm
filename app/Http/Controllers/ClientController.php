<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = 10;
        $clients = Client::orderBy('id', 'desc')->paginate($pages);
        return view('client.index', compact('clients'))
            ->with('i', (request()->input('page',1) -1)* $pages );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $client = Client::create($request->all());

        if($request->hasFile('avatar'))
        {

            $avatar_filename = 'avatar_user_'.$client->id.'.'.request()->avatar->getClientOriginalExtension();

            $request->file('avatar')->storeAs(
                'avatars', $avatar_filename
            );

            // Updates avatar filename
            $client->avatar = $avatar_filename;
            $client->save();
        }

        return redirect()->route('client.index')->with('success', 'New client created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('client.show',compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {

        return view('client.edit', compact('client'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $client->update($request->all());

        if($request->hasFile('avatar'))
        {

            $avatar_filename = 'avatar_user_'.$client->id.'.'.request()->avatar->getClientOriginalExtension();

            $request->file('avatar')->storeAs(
                'avatars', $avatar_filename
            );

            // Updates avatar filename
            $clientdata = Client::find($client->id);
            $clientdata->avatar = $avatar_filename;
            $clientdata->save();
        }


        return redirect()->route('client.index')->with('success', 'Client updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client $client
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Client $client)
    {
        $client->delete();
        return redirect()->route('client.index')->with('success', 'Client deleted successfully');
    }
}
