laravel-miniCRM
===============

This is a miniCRM that was build on laravel for basic clients/transactions management.

Install
-------

Make sure every php extension is installed that is required to run laravel,
Here is the official laravel installation guide:
https://laravel.com/docs/5.8/installation

Clone the project public repository using the following command:

```bash
git clone git@bitbucket.org:optiopus/laravel-crm.git
```

Copy .env.example to .env.

```bash
cd laravel-crm
cp .env.example .env
```

Create an empty database named:

**laravelcrm**

NOTE: The db name can be changed from DB_DATABASE variable in the .env file.
      Currently it is configured to be used with mysql, this can be changed from config/database.php  

To start the PHP project in dev environment, do the following:

```bash

**Update composer dependencies** 
composer update

**Install npm dependencies** 
npm install

**Run the migrations to insert sample migrations into db** 
php artisan migrate:fresh --seed

**Symlink storage folder** 
php artisan storage:link

**Run watcher to compile javascripts/css** 
npm run watch

**Run build in php server (host and port can be changed according to the prefered configurations)** 
php artisan serve --host 0.0.0.0 --port 8003

```

When deploy the PHP project in production environment, do the following:

```bash

**Install composer dependencies** 
composer install

**Install npm dependencies** 
npm install

**Run the migrations** 
php artisan migrate

**Symlink storage folder** 
php artisan storage:link

**Compile production ready javascripts/css** 
npm run production

```
