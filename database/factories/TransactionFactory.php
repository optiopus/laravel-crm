<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Transaction;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    return [
        'client_id'=> $faker->numberBetween(1, 24),
        'amount' => $faker->numberBetween(25, 850),
    ];
});
