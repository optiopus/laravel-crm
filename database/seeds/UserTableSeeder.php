<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = new User;
        $admin->name = 'Administrator';
        $admin->email = 'admin@admin.com';
        $admin->password = Hash::make('password');
        $admin->save();
        $admin->roles()->attach(Role::where('name', 'admin')->first());
    }
}
