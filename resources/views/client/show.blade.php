@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-5 mx-auto">
                <h3>Client Details</h3>
                <hr>
                <div class="card text-center mb-4">
                    @if(empty($client->avatar))
                        <img class="card-img-top rounded-circle mx-auto d-block w-50"
                             src="{{url('img/avatar/default.jpg')}}" alt="Card image cap">
                    @else
                        <img class="card-img-top rounded-circle mx-auto d-block w-50"
                             src="{{url('storage/avatars')}}/{{$client->avatar}}" alt="Card image cap">
                    @endif
                    <div class="card-body">
                        <h5 class="card-title">{{$client->first_name}} {{$client->last_name}}</h5>
                        <p class="card-text">{{$client->email}}.</p>
                    </div>
                    <div class="card-body">
                        <a href="{{route('client.index')}}" class="btn btn-sm btn-danger">Back</a>
                    </div>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseTransactions" aria-expanded="false" aria-controls="collapseTransactions">
                        View Transactions
                    </button>
                </div>
            </div>
            <div class="col-lg-7 collapse" id="collapseTransactions">
                <div >
                    <h3>Transactions</h3>
                    <hr>
                    @if ($transactions = App\Transaction::where('client_id', '=', $client->id)->get())

                        <table class="table table-hover table-bordered table-sm">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Date</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($transactions as $transaction)
                                <tr>
                                    <td>{{$transaction->id}}</td>
                                    <td>{{$transaction->amount}}</td>
                                    <td>{{$transaction->created_at}}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection