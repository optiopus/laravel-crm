@extends('layouts.app')
@section('content')
    <div class="container">

        <h3>New client</h3>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{route('client.store')}}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="email">Email :</label>
               <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            </div>
            <div class="form-group">
                <label for="first_name">First Name :</label>
                <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>
            </div>
            <div class="form-group">
                <label for="last_name">Last Name :</label>
                <input type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>
            </div>
            <div class="form-group">
                <input type="file" class="form-control-file" name="avatar" >
                <small class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                <a href="{{route('client.index')}}" class="btn btn-sm btn-danger">Cancel</a>
            </div>

        </form>

    </div>
@endsection