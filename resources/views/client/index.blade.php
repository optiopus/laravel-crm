@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="p-2">
                <h3>Clients</h3>
            </div>
            <div class="p-2">
                <a class="btn btn-sm btn-success mb-4" href="{{ route('client.create') }}"><i class="far fa-plus-square"> New</i></a>
            </div>
        </div>

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{$message}}</p>
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-hover table-sm">
                <thead>
                    <tr>
                        <th width="5%" scope="col">#</th>
                        <th width="20%" scope="col">First Name</th>
                        <th width="20%" scope="col">Last Name</th>
                        <th class="d-none d-sm-block" width="20%" scope="col">Email</th>
                        <th width="35%" scope="col" class="text-right">Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($clients as $client)
                        <tr>
                            <th scope="row">{{$client->id}}</th>
                            <td>{{$client->first_name}}</td>
                            <td>{{$client->last_name}}</td>
                            <td class="d-none d-sm-block">{{$client->email}}</td>
                            <td class="text-right">
                                <form action="{{ route('client.destroy', $client->id) }}" method="post">
                                    <a class="btn btn-sm btn-primary" href="{{route('client.show',$client->id)}}"><i class="far fa-eye"></i></a>
                                    <a class="btn btn-sm btn-warning" href="{{route('client.edit',$client->id)}}"><i class="far fa-edit"></i></a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        <div>

        {!! $clients->links() !!}
    </div>
@endsection