@extends('layouts.app')
@section('content')
    <div class="container">

        <h3>Edit client</h3>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{route('client.update',$client->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="email">Email :</label>
                <input type="email" value="{{$client->email}}" class="form-control @error('email') is-invalid @enderror" name="email" required autocomplete="email" autofocus>
            </div>
            <div class="form-group">
                <label for="first_name">First Name :</label>
                <input type="text" value="{{$client->first_name}}" class="form-control @error('first_name') is-invalid @enderror" name="first_name" required autocomplete="first_name" autofocus>
            </div>
            <div class="form-group">
                <label for="last_name">Last Name :</label>
                <input type="text" value="{{$client->last_name}}" class="form-control @error('last_name') is-invalid @enderror" name="last_name" required autocomplete="last_name" autofocus>
            </div>
            <div class="form-group">
                <input type="file" class="form-control-file" name="avatar" value="{{$client->avatar}}" >
                <small class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-sm btn-success">Save</button>
                <a href="{{route('client.index')}}" class="btn btn-sm btn-danger">Cancel</a>
            </div>
        </form>
    </div>
@endsection