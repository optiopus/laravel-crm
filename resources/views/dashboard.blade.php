@extends('layouts.app')

@section('content')
<div class="container">
    <div class="jumbotron">
        <div class="container">
            <h2>Hello, {{ auth()->user()->name }} !</h2>
            <p>Please choose your path :)</p>
        </div>
    </div>
    <div class="card">
        <div class="card-header">Dashboard</div>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="row text-center">
                <div class="col-md-4">
                    <a href="{{ route('client.index') }}">
                        <i class="fas fa-users fa-7x"></i>
                        <h2 class="mb-5">{{ __('Clients') }}</h2>
                    </a>
                </div>
                <div class="col-md-4">
                    <a class="text-success" href="{{ route('transaction.index') }}">
                        <i class="fas fa-book-open fa-7x"></i>
                        <h2 class="mb-5">{{ __('Transactions') }}</h2>
                    </a>
                </div>
                <div class="col-md-4">
                    <a class="text-danger" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt fa-7x"></i>
                        <h2 class="mb-5">{{ __('Logout') }}</h2>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
