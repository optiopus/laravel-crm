@extends('layouts.app')
@section('content')
    <div class="container">

        <h3>Edit transaction</h3>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{route('transaction.update',$transaction->id)}}" method="post">
            @csrf
            @method('PUT')

            @if ($clients = App\Client::all())
                <label for="client_id">Client ID :</label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="client_id">Client</label>
                    </div>
                    <select name="client_id" class="custom-select @error('client_id') is-invalid @enderror">
                        <option value="{{$transaction->client_id}}" selected>Current</option>

                        @foreach ($clients as $client)
                            <option value="{{ $client->id }}">{{ $client->id }} - {{ $client->first_name }} {{ $client->last_name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            <div class="form-group">
                <label for="amount">Amount :</label>
                <input type="number" min="0.00" max="999999.99" step="0.01" value="{{$transaction->amount}}" class="form-control @error('amount') is-invalid @enderror" name="amount" placeholder="xxxx.xx" pattern="\d+(\.\d{2})?" autocomplete="amount" autofocus>
            </div>
            <div class="form-group">
                <label for="created_at">Transaction Date :</label>
                <input type="text" value="{{$transaction->created_at}}" id="created_at" class="form-control @error('created_at') is-invalid @enderror" name="created_at" required autocomplete="created_at" autofocus>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-sm btn-success">Save</button>
                <a href="{{route('transaction.index')}}" class="btn btn-sm btn-danger">Cancel</a>
            </div>
        </form>
    </div>
@endsection