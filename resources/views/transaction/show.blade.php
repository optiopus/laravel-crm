@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-5 mx-auto">
                <h3>Transaction Details</h3>
                <hr>
                <div class="card">
                    <div class="card-header text-right">
                        <a href="{{route('transaction.index')}}" class="btn btn-sm btn-danger"><i class="fas fa-times"></i></a>
                    </div>
                    <div class="card-body">
                        @if(isset($transaction->client_id))
                            <h5 class="card-title"><strong>Client</strong></h5>
                            @if ($client = App\Client::find($transaction->client_id))
                                <h6 class="card-title"><strong>ID: </strong>{{ $client->id }}</h6>
                                <h6 class="card-title"><strong>Email: </strong>{{ $client->email }}</h6>
                                <h6 class="card-title mb-4"><strong>Name: </strong>
                                    <a class="text-dark" href="{{route('client.show',$transaction->client_id)}}"
                                       target="_blank">{{ $client->first_name }} {{ $client->last_name }}
                                    </a>
                                </h6>
                            @endif
                        @endif
                        <h5 class="card-title"><strong>Transaction</strong></h5>
                        <h6 class="card-title"><strong>Date: </strong>{{$transaction->created_at->format('d M Y')}}</h6>
                        <h6 class="card-title"><strong>Amount: </strong>{{$transaction->amount}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection