@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="p-2">
                <h3>Transactions</h3>
            </div>
            <div class="p-2">
                <a class="btn btn-sm btn-success mb-4" href="{{ route('transaction.create') }}"><i class="far fa-plus-square"> New</i></a>
            </div>
        </div>

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{$message}}</p>
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-hover table-sm">
                <thead>
                    <tr>
                        <th width="5%" scope="col">#</th>
                        <th class="d-none d-sm-block" width="20%" scope="col">Client</th>
                        <th width="20%" scope="col">Amount</th>
                        <th width="20%" scope="col">Date</th>
                        <th width="35%" scope="col" class="text-right">Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($transactions as $transaction)
                        <tr>
                            <th scope="row">{{$transaction->id}}</th>
                            <td class="d-none d-sm-block">
                                @if(isset($transaction->client_id))
                                    <a class="text-dark" href="{{route('client.show',$transaction->client_id)}}" target="_blank"    >
                                        @if ($client = App\Client::find($transaction->client_id)) @endif
                                        {{ $client->first_name }} {{ $client->last_name }}
                                    </a>
                                @endif
                            </td>
                            <td>{{$transaction->amount}}</td>
                            <td>{{$transaction->created_at->format('d M Y')}}</td>
                            <td class="text-right">
                                <form action="{{ route('transaction.destroy', $transaction->id) }}" method="post">
                                    <a class="btn btn-sm btn-primary" href="{{route('transaction.show',$transaction->id)}}"><i class="far fa-eye"></i></a>
                                    <a class="btn btn-sm btn-warning" href="{{route('transaction.edit',$transaction->id)}}"><i class="far fa-edit"></i></a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        <div>

        {!! $transactions->links() !!}
    </div>
@endsection