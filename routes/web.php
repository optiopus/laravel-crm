<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/admin', 'AdminController@index')->name('admin');
Route::get('/admin/dashboard', 'DashboardController@index')->name('admin/dashboard');
Route::resource('admin/client','ClientController');
Route::resource('admin/transaction','TransactionController');


